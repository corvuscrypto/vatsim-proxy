package fsd

import (
	"bytes"
	"strconv"
	"strings"
)

const PROTOCOL_VERSION_MAJOR = "3"
const PROTOCOL_VERSION_MINOR = "2"

type password string

func (p password) String() string {
	return "********"
}

// PacketType represents the packet type received
type PacketType rune

// Valid Packet types
const (
	RequestResponse PacketType = '$'
	GeneralUpdate   PacketType = '#'
	ATCUpdate       PacketType = '%'
	AircraftUpdate  PacketType = '@'
)

func (p PacketType) String() string {
	if p == RequestResponse {
		return "RequestResponse"
	}
	if p == GeneralUpdate {
		return "GeneralUpdate"
	}
	if p == ATCUpdate {
		return "ATCUpdate"
	}
	if p == AircraftUpdate {
		return "AircraftUpdate"
	}
	return "Unknown"
}

// Packet is the primary interface which
type Packet interface {
	Marshal() ([]byte, error)
	Unmarshal([]byte) error
}

// PacketBase is the base structure of a packet
type PacketBase struct {
	Type        PacketType
	Command     string
	Source      string
	Destination string
}

// GeneratePrefix created the initial prefix used by all packets
func (p *PacketBase) GeneratePrefix() (data []byte) {
	holder := make([][]byte, 2)
	preamble := make([]byte, 0)
	preamble = append(preamble, byte(p.Type))
	preamble = append(preamble, []byte(p.Command)...)
	preamble = append(preamble, []byte(p.Source)...)

	holder[0] = preamble
	holder[1] = []byte(p.Destination)

	return bytes.Join(holder, []byte{':'})
}

// ReadPrefix reads in the basic info from a packet
func (p *PacketBase) ReadPrefix(data []byte) (n int) {
	p.Type = PacketType(data[0])
	p.Command = string(data[1:3])
	buffer := make([]byte, 64)
	var offset = 3
	for i, _byte := range data[offset:] {
		if _byte == ':' {
			p.Source = string(buffer[:i])
			offset += i + 1
			break
		}
		buffer[i] = _byte
	}

	p.Destination = string(bytes.SplitN(data[offset:], []byte{':'}, 2)[0])
	return offset + len(p.Destination) + 1
}

// ServerConnectionResponse is the first packet returned from the vatsim servers
type ServerConnectionResponse struct {
	PacketBase
	Name  string
	Token string
}

// Marshal generates a byte array from the data in the packet structure
func (p *ServerConnectionResponse) Marshal() (data []byte, err error) {
	holder := make([][]byte, 3)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Name)
	holder[2] = []byte(p.Token)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *ServerConnectionResponse) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := bytes.Split(data[offset:], []byte{':'})
	p.Name = string(splits[0])
	p.Token = string(splits[1])
	return
}

// ClientConnectionResponse is the first packet returned from the vatsim servers
type ClientConnectionResponse struct {
	PacketBase
	ClientID   string
	ClientName string
	NetworkID  string
	Nonce      string
}

// Marshal generates a byte array from the data in the packet structure
func (p *ClientConnectionResponse) Marshal() (data []byte, err error) {
	holder := make([][]byte, 7)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.ClientID)
	holder[2] = []byte(p.ClientName)
	holder[3] = []byte(p.NetworkID)
	holder[4] = []byte(PROTOCOL_VERSION_MAJOR)
	holder[5] = []byte(PROTOCOL_VERSION_MINOR)
	holder[6] = []byte(p.Nonce)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *ClientConnectionResponse) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := bytes.Split(data[offset:], []byte{':'})
	p.ClientID = string(splits[0])
	p.ClientName = string(splits[1])
	p.NetworkID = string(splits[2])
	p.Nonce = string(splits[5])
	return
}

// ATCLogin is the login packet for ATC only
type ATCLogin struct {
	PacketBase
	FullName   string
	NetworkID  string
	Password   password
	PositionID string
	Version    string
}

// Marshal generates a byte array from the data in the packet structure
func (p *ATCLogin) Marshal() (data []byte, err error) {
	holder := make([][]byte, 6)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.FullName)
	holder[2] = []byte(p.NetworkID)
	holder[3] = []byte(p.Password)
	holder[4] = []byte(p.PositionID)
	holder[5] = []byte(p.Version)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *ATCLogin) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := bytes.SplitN(data[offset:], []byte{':'}, 5)
	p.FullName = string(splits[0])
	p.NetworkID = string(splits[1])
	p.Password = password(splits[2])
	p.PositionID = string(splits[3])
	p.Version = string(splits[4])
	return
}

// PilotLogin is the login packet for Pilots only, because of course it had to be different
type PilotLogin struct {
	PacketBase
	NetworkID  string
	Password   string
	PositionID string
	Version    string
	AirlineID  string
	FullName   string
}

// Marshal generates a byte array from the data in the packet structure
func (p *PilotLogin) Marshal() (data []byte, err error) {
	holder := make([][]byte, 7)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.NetworkID)
	holder[2] = []byte(p.Password)
	holder[3] = []byte(p.PositionID)
	holder[4] = []byte(p.Version)
	holder[5] = []byte(p.AirlineID)
	holder[6] = []byte(p.FullName)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *PilotLogin) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := bytes.SplitN(data[offset:], []byte{':'}, 6)
	p.NetworkID = string(splits[0])
	p.Password = string(splits[1])
	p.PositionID = string(splits[2])
	p.Version = string(splits[3])
	p.AirlineID = string(splits[4])
	p.FullName = string(splits[5])
	return
}

// PrivateMessage is the login packet for Pilots only, because of course it had to be different
type PrivateMessage struct {
	PacketBase
	Message string
}

// Marshal generates a byte array from the data in the packet structure
func (p *PrivateMessage) Marshal() (data []byte, err error) {
	holder := make([][]byte, 2)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Message)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *PrivateMessage) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	p.Message = string(data[offset:])
	return
}

// QueryRequest is the packet for indicating there is a challenge-response request incoming
type QueryRequest struct {
	PacketBase
	Type string
}

// Marshal generates a byte array from the data in the packet structure
func (p *QueryRequest) Marshal() (data []byte, err error) {
	holder := make([][]byte, 2)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *QueryRequest) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	p.Type = string(data[offset:])
	return
}

// QueryResponse is the packet for sending challenge/response data
type QueryResponse struct {
	PacketBase
	Type string
	Data string
}

// Marshal generates a byte array from the data in the packet structure
func (p *QueryResponse) Marshal() (data []byte, err error) {
	holder := make([][]byte, 3)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	holder[2] = []byte(p.Data)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *QueryResponse) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := bytes.SplitN(data[offset:], []byte{':'}, 2)
	p.Type = string(splits[0])
	p.Data = string(splits[1])
	return
}

// RemoveClient is the packet for removing a client
type RemoveClient struct {
	PacketBase
	NetworkID string
}

// Marshal generates a byte array from the data in the packet structure
func (p *RemoveClient) Marshal() (data []byte, err error) {
	holder := make([][]byte, 2)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.NetworkID)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *RemoveClient) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	p.NetworkID = string(data[offset:])
	return
}

// UpdatePlanePosition is the packet for updating an aircraft's position
type UpdatePlanePosition struct {
	PacketBase
	Mode        string
	Callsign    string
	Squawk      int
	Rating      string
	Latitude    float64
	Longitude   float64
	Altitude    int
	Groundspeed int
	Unknown1    string
	Unknown2    string
}

// Marshal generates a byte array from the data in the packet structure
func (p *UpdatePlanePosition) Marshal() (data []byte, err error) {
	holder := make([][]byte, 11)

	holder[0] = []byte("@")
	holder[1] = []byte(p.Mode)
	holder[2] = []byte(p.Callsign)
	holder[3] = []byte(strconv.Itoa(p.Squawk))
	holder[4] = []byte(p.Rating)
	holder[5] = []byte(strconv.FormatFloat(p.Latitude, 'f', 5, 32))
	holder[6] = []byte(strconv.FormatFloat(p.Longitude, 'f', 5, 32))
	holder[7] = []byte(strconv.Itoa(p.Altitude))
	holder[8] = []byte(strconv.Itoa(p.Groundspeed))
	holder[9] = []byte(p.Unknown1)
	holder[10] = []byte(p.Unknown2)

	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *UpdatePlanePosition) Unmarshal(data []byte) (err error) {
	p.PacketBase.Type = AircraftUpdate
	splits := strings.Split(string(data[1:]), ":")
	p.Mode = splits[0]
	p.Callsign = splits[1]
	p.Squawk, _ = strconv.Atoi(splits[2])
	p.Rating = splits[3]
	p.Latitude, _ = strconv.ParseFloat(splits[4], 32)
	p.Longitude, _ = strconv.ParseFloat(splits[5], 32)
	p.Altitude, _ = strconv.Atoi(splits[6])
	p.Groundspeed, _ = strconv.Atoi(splits[7])
	p.Unknown1 = splits[8]
	p.Unknown2 = splits[9]
	return
}

// UpdateATCInfo is the packet for updating ATC's info
type UpdateATCInfo struct {
	PacketBase
	Callsign        string
	Freq            string
	Altitude        int
	ProtocolVersion string
	Rating          string
	Latitude        float64
	Longitude       float64
}

// Marshal generates a byte array from the data in the packet structure
func (p *UpdateATCInfo) Marshal() (data []byte, err error) {
	holder := make([][]byte, 8)

	holder[0] = []byte("%")
	holder[1] = []byte(p.Callsign)
	holder[2] = []byte(p.Freq)
	holder[3] = []byte(strconv.Itoa(p.Altitude))
	holder[4] = []byte(p.ProtocolVersion)
	holder[5] = []byte(p.Rating)
	holder[6] = []byte(strconv.FormatFloat(p.Latitude, 'f', 5, 32))
	holder[7] = []byte(strconv.FormatFloat(p.Longitude, 'f', 5, 32))

	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *UpdateATCInfo) Unmarshal(data []byte) (err error) {
	p.PacketBase.Type = ATCUpdate
	splits := strings.Split(string(data[1:]), ":")
	p.Callsign = splits[0]
	p.Freq = splits[1]
	p.Altitude, _ = strconv.Atoi(splits[2])
	p.ProtocolVersion = splits[3]
	p.Rating = splits[4]
	p.Latitude, _ = strconv.ParseFloat(splits[5], 32)
	p.Longitude, _ = strconv.ParseFloat(splits[6], 32)
	return
}

// WeatherRequest is the packet for requesting weather information
type WeatherRequest struct {
	PacketBase
	Type    string
	Airport string
}

// Marshal generates a byte array from the data in the packet structure
func (p *WeatherRequest) Marshal() (data []byte, err error) {
	holder := make([][]byte, 3)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	holder[2] = []byte(p.Airport)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *WeatherRequest) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.Type = splits[0]
	p.Airport = splits[1]
	return
}

// WeatherResponse is the packet for getting the response of metar info
type WeatherResponse struct {
	PacketBase
	Type string
	Data string
}

// Marshal generates a byte array from the data in the packet structure
func (p *WeatherResponse) Marshal() (data []byte, err error) {
	holder := make([][]byte, 3)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	holder[2] = []byte(p.Data)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *WeatherResponse) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.Type = splits[0]
	p.Data = splits[1]
	return
}

// Heartbeat is the packet for getting the response of metar info
type Heartbeat struct {
	PacketBase
	Unknown1 string
	Unknown2 string
}

// Marshal generates a byte array from the data in the packet structure
func (p *Heartbeat) Marshal() (data []byte, err error) {
	holder := make([][]byte, 3)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Unknown1)
	holder[2] = []byte(p.Unknown2)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *Heartbeat) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.Unknown1 = splits[0]
	p.Unknown2 = splits[1]
	return
}

// PingRequest is the packet for getting the response of metar info
type PingRequest struct {
	PacketBase
	Hash string
}

// Marshal generates a byte array from the data in the packet structure
func (p *PingRequest) Marshal() (data []byte, err error) {
	holder := make([][]byte, 2)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Hash)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *PingRequest) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	p.Hash = string(data[offset:])
	return
}

// PingResponse is the packet for getting the response of metar info
type PingResponse struct {
	PacketBase
	Hash string
}

// Marshal generates a byte array from the data in the packet structure
func (p *PingResponse) Marshal() (data []byte, err error) {
	holder := make([][]byte, 2)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Hash)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *PingResponse) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	p.Hash = string(data[offset:])
	return
}

// Disconnect is the packet for getting the response of metar info
type Disconnect struct {
	PacketBase
}

// Marshal generates a byte array from the data in the packet structure
func (p *Disconnect) Marshal() (data []byte, err error) {
	return p.GeneratePrefix(), nil
}

// Unmarshal parses data in byte array into the struct
func (p *Disconnect) Unmarshal(data []byte) (err error) {
	p.ReadPrefix(data)
	return
}

// UpdateFlightPlan is the packet for getting the response of metar info
type UpdateFlightPlan struct {
	PacketBase
	Type                string
	Aircraft            string
	Origin              string
	ScheduledDeparture1 string
	ScheduledDeparture2 string
	Altitude            int
	Destination         string
	ETE                 string
	Fuel                string
	Alternate           string
	Remarks             string
}

// Marshal generates a byte array from the data in the packet structure
func (p *UpdateFlightPlan) Marshal() (data []byte, err error) {
	holder := make([][]byte, 12)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	holder[2] = []byte(p.Aircraft)
	holder[3] = []byte(p.Origin)
	holder[4] = []byte(p.ScheduledDeparture1)
	holder[5] = []byte(p.ScheduledDeparture2)
	holder[6] = []byte(strconv.Itoa(p.Altitude))
	holder[7] = []byte(p.Destination)
	holder[8] = []byte(p.ETE)
	holder[9] = []byte(p.Fuel)
	holder[10] = []byte(p.Alternate)
	holder[11] = []byte(p.Remarks)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *UpdateFlightPlan) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.Type = splits[0]
	p.Aircraft = splits[1]
	p.Origin = splits[2]
	p.ScheduledDeparture1 = splits[3]
	p.ScheduledDeparture2 = splits[4]
	p.Altitude, _ = strconv.Atoi(splits[5])
	p.Destination = splits[6]
	p.ETE = splits[7]
	p.Fuel = splits[8]
	p.Alternate = splits[9]
	p.Remarks = splits[10]
	return
}

// AcknowledgeFlightPlan is the packet for getting the response of metar info
type AcknowledgeFlightPlan struct {
	PacketBase
	Type     string
	Subtype  string
	Callsign string
}

// Marshal generates a byte array from the data in the packet structure
func (p *AcknowledgeFlightPlan) Marshal() (data []byte, err error) {
	holder := make([][]byte, 12)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.Type)
	holder[2] = []byte(p.Subtype)
	holder[3] = []byte(p.Callsign)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *AcknowledgeFlightPlan) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.Type = splits[0]
	if len(splits) > 1 {
		p.Subtype = splits[1]
	}
	return
}

// ErrorMessage is the packet for getting the response of metar info
type ErrorMessage struct {
	PacketBase
	ErrorCode string
	Callsign  string
	Message   string
}

// Marshal generates a byte array from the data in the packet structure
func (p *ErrorMessage) Marshal() (data []byte, err error) {
	holder := make([][]byte, 12)

	holder[0] = p.GeneratePrefix()
	holder[1] = []byte(p.ErrorCode)
	holder[2] = []byte(p.Callsign)
	holder[3] = []byte(p.Message)
	return bytes.Join(holder, []byte{':'}), nil
}

// Unmarshal parses data in byte array into the struct
func (p *ErrorMessage) Unmarshal(data []byte) (err error) {
	offset := p.ReadPrefix(data)
	splits := strings.Split(string(data[offset:]), ":")
	p.ErrorCode = splits[0]
	p.Callsign = splits[1]
	p.Message = splits[2]
	return
}
