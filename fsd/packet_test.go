package fsd

import "testing"

func TestPacketTypes(t *testing.T) {
	if RequestResponse.String() != "RequestResponse" {
		t.Fail()
	}
	if GeneralUpdate.String() != "GeneralUpdate" {
		t.Fail()
	}
	if ATCUpdate.String() != "ATCUpdate" {
		t.Fail()
	}
	if AircraftUpdate.String() != "AircraftUpdate" {
		t.Fail()
	}
	if PacketType('p').String() != "Unknown" {
		t.Fail()
	}
}

func TestPacketBase(t *testing.T) {
	data := []byte("$DIServer:Client")
	packet := new(PacketBase)

	packet.ReadPrefix(data)

	if packet.Type != RequestResponse {
		t.Errorf("Packet type incorrect: %c\n", packet.Type)
	}

	if packet.Command != "DI" {
		t.Errorf("Packet destination incorrect: %s\n", packet.Command)
	}

	if packet.Source != "Server" {
		t.Errorf("Packet destination incorrect: %s\n", packet.Destination)
	}

	if packet.Destination != "Client" {
		t.Errorf("Packet source incorrect: %s\n", packet.Source)
	}

	newData := packet.GeneratePrefix()

	if string(newData) != string(data) {
		t.Errorf("Serialization incorrect: %s", newData)
	}

}

func TestServerConnectionResponse(t *testing.T) {
	data := []byte("$DIServer:Client:VATSIM FSD V3.13:abcdefabcdefabcdefabcd")
	packet := new(ServerConnectionResponse)

	packet.Unmarshal(data)

	if packet.Type != RequestResponse {
		t.Errorf("Packet type incorrect: %c\n", packet.Type)
	}

	if packet.Command != "DI" {
		t.Errorf("Packet destination incorrect: %s\n", packet.Command)
	}

	if packet.Destination != "Server" {
		t.Errorf("Packet destination incorrect: %s\n", packet.Destination)
	}

	if packet.Source != "Client" {
		t.Errorf("Packet source incorrect: %s\n", packet.Source)
	}

	if packet.Name != "VATSIM FSD V3.13" {
		t.Errorf("Packet name incorrect: %s\n", packet.Name)
	}

	if packet.Token != "abcdefabcdefabcdefabcd" {
		t.Errorf("Packet token incorrect: %s\n", packet.Token)
	}

	newData, _ := packet.Marshal()
	if string(newData) != string(data) {
		t.Errorf("Packet marshalled incorrectly: %s\n", newData)
	}

}
