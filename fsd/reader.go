package fsd

import (
	"bufio"
	"io"
	"log"
)

func packetByCommand(data []byte) (packet Packet) {
	switch string(data[1:3]) {
	case "DI":
		packet := new(ServerConnectionResponse)
		packet.Unmarshal(data)
		return packet
	case "ID":
		packet := new(ClientConnectionResponse)
		packet.Unmarshal(data)
		return packet
	case "AA":
		packet := new(ATCLogin)
		packet.Unmarshal(data)
		return packet
	case "AP":
		packet := new(PilotLogin)
		packet.Unmarshal(data)
		return packet
	case "TM":
		packet := new(PrivateMessage)
		packet.Unmarshal(data)
		return packet
	case "CQ":
		packet := new(QueryRequest)
		packet.Unmarshal(data)
		return packet
	case "CR":
		packet := new(QueryResponse)
		packet.Unmarshal(data)
		return packet
	case "AX":
		packet := new(WeatherRequest)
		packet.Unmarshal(data)
		return packet
	case "AR":
		packet := new(WeatherResponse)
		packet.Unmarshal(data)
		return packet
	case "DA", "DP":
		packet := new(Disconnect)
		packet.Unmarshal(data)
		return packet
	case "PC", "SB":
		packet := new(AcknowledgeFlightPlan)
		packet.Unmarshal(data)
		return packet
	case "FP":
		packet := new(UpdateFlightPlan)
		packet.Unmarshal(data)
		return packet
	case "ER":
		packet := new(ErrorMessage)
		packet.Unmarshal(data)
		return packet
	case "ZC":
		packet := new(PingRequest)
		packet.Unmarshal(data)
		return packet
	case "ZR":
		packet := new(PingResponse)
		packet.Unmarshal(data)
		return packet
	case "DL":
		packet := new(Heartbeat)
		packet.Unmarshal(data)
		return packet

	default:
		log.Println("Unknown packet", string(data))
		return nil
	}
}

// ReadPacket takes data and parses the data
func ReadPacket(data []byte) (packet Packet) {
	switch PacketType(data[0]) {
	case AircraftUpdate:
		packet = new(UpdatePlanePosition)
		packet.Unmarshal(data)
		return
	case ATCUpdate:
		packet = new(UpdateATCInfo)
		packet.Unmarshal(data)
		return
	default:
		return packetByCommand(data)
	}
}

// Reader is the primary reader to use
type Reader struct {
	bytesScanner *bufio.Scanner
}

// Next attempts to get the next packet, if there is no data available then
// an EOF error is thrown
func (reader *Reader) Next() (packet Packet, err error) {
	if reader.bytesScanner.Scan() {
		return ReadPacket(reader.bytesScanner.Bytes()), reader.bytesScanner.Err()
	}

	return nil, io.EOF
}

// NewReader creates a new FSD reader
func NewReader(source io.Reader) (reader *Reader) {
	reader = new(Reader)
	reader.bytesScanner = bufio.NewScanner(source)
	return
}
