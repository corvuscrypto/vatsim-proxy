package main

import (
	"log"
	"net"
)

var globalConn *Connection

func listen() {
	log.Println("starting server")
	address, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:6809")
	listener, _ := net.ListenTCP("tcp", address)
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			continue
		}
		if globalConn != nil {
			globalConn.Close()
		}
		globalConn = NewConnection(conn)

	}
}
