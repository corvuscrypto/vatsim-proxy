package main

import (
	"bytes"
	"io"
	"log"
	"net"

	"gitlab.com/corvuscrypto/vatsim-proxy/fsd"
)

// VatsimAddr is the default vatsim addr
var VatsimAddr, gerr = net.ResolveTCPAddr("tcp", "165.22.163.56:6809")

// Connection to read/write from
type Connection struct {
	remoteConnection *net.TCPConn
	localConnection  *net.TCPConn
	proxyChannel     chan []byte
	eofCount         int
	isClosed         bool
}

func (c *Connection) beginLoop(connInput, connOutput *net.TCPConn) {
	for {
		buffer := make([]byte, 256)
		n, err := connInput.Read(buffer)
		if err != nil {
			if err == io.EOF {
				log.Println("closing connection", c)
				c.proxyChannel <- buffer[:n]
				connOutput.Write(buffer[:n])
				connInput.Close()
				c.eofCount++
				if c.eofCount == 2 {
					close(c.proxyChannel)
					c.isClosed = true
				}
				return
			}
		}
		connOutput.Write(buffer[:n])
		c.proxyChannel <- buffer[:n]
	}
}

func (c *Connection) startLogger() {
	bufferBytes := make([]byte, 0)
	buffer := bytes.NewBuffer(bufferBytes)
	reader := fsd.NewReader(buffer)
	for {
		data := <-c.proxyChannel
		buffer.Write(data)

		packet, err := reader.Next()
		if err == io.EOF {
			continue
		}
		// out, _ := packet.Marshal()
		log.Println(packet)
	}
}

func (c *Connection) Close() {
	defer func() {
		recover()
	}()
	c.localConnection.Close()
	c.remoteConnection.Close()
	close(c.proxyChannel)
}

// NewConnection creates a new proxy connection
func NewConnection(conn *net.TCPConn) *Connection {
	connection := new(Connection)
	connection.localConnection = conn
	remote, err := net.DialTCP("tcp4", nil, VatsimAddr)
	log.Println(err, remote)
	connection.remoteConnection = remote
	connection.proxyChannel = make(chan []byte, 10)
	go connection.beginLoop(connection.localConnection, connection.remoteConnection)
	go connection.beginLoop(connection.remoteConnection, connection.localConnection)
	go connection.startLogger()
	return connection
}
